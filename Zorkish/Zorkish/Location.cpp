#include "Location.h"

void Location::AddLocation(Location* newLocation, int direction)
{
	_connectedNotes[direction] = newLocation;
}

Location* Location::GetLocation(Direction direction)
{
	if (_connectedNotes[direction] != nullptr)
	{
		return _connectedNotes[direction];
	}

	return nullptr;
}

int Location::GetId()
{
	return _id;
}

void Location::PrintConnections()
{
	for (unsigned int i = 0; i < 4; i++)
	{
		if (_connectedNotes[i] != nullptr)
		{
			cout << "Door at ";

			switch (i)
			{
			case 0: cout << "North ";
				break;
			case 1: cout << "South ";
				break;
			case 2: cout << "East";
				break;
			case 3: cout << "West";
				break;
			default:
				break;
			}

			cout << endl;
		}
	}
}

Location::Location(int id, string name, string description)
{
	_name = name;
	_id = id;
	_description = description;
}

void Location::Look()
{
	cout << GetDescription() << endl;
	PrintConnections();

	for (auto child: GetChilds())
	{
		cout << "You see a " << child->GetName() << endl;
	}
}