#pragma once

#include "StateBase.h"

class Help :
	public StateBase
{
private:
	Help() {};
public:
	static Help* GetInstance();
	void Show(StateManager* context);
};

