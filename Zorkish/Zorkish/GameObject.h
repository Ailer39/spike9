#pragma once
#include <string>
#include <iostream>
#include <vector>

using namespace std;

class GameObject
{
private:
	vector<GameObject*> _gameObjects;
protected:
	GameObject() {};
	void SetName(string name);
	string _name;
	string _description;
	vector<GameObject*> GetChilds();
public:
	string GetName();
	GameObject(string name, string description);
	string GetDescription();
	void AddGameObject(GameObject* gameObj);
	void RemoveGameObject(string name);
	GameObject* GetGameObject(string name);
	void LookIn();
	void LookAt();
};