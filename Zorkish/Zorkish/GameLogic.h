#pragma once
#include "StateBase.h"
#include "Location.h"
#include "Player.h"
#include "CommandManager.h"

class GameLogic :
	public StateBase
{
private:
	GameLogic() {};
	Player* _player;
	CommandManager* _commandManager;
	bool _gameFinished;
public:
	static GameLogic* GetInstance();
	void InitLevel(Location* startLocation);
	void Show(StateManager* context);
};