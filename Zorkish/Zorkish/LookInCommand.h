#pragma once
#include "BaseCommand.h"
#include "Location.h"
#include "Player.h"

class LookInCommand :
	public BaseCommand
{
public:

	void execute(Location* currentLocation = nullptr, string cmdText = "")
	{
		int index = cmdText.find("in") + 2;
		string objName = cmdText.substr(index, cmdText.size());
		objName = objName.erase(0, objName.find_first_not_of(" "));
		objName = objName.erase(objName.find_last_not_of(" ") + 1);
		GameObject* gObj = currentLocation->GetGameObject(objName);

		if (gObj != nullptr)
		{
			gObj->LookIn();
		}
	}
};