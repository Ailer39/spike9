#include "Help.h"

static Help* instance;

Help* Help::GetInstance()
{
	if (instance == nullptr)
	{
		instance = new Help();
	}

	return instance;
}

void Help::Show(StateManager* context)
{
	cout << "Zorkish :: Help" << endl;
	cout << "The following commands are supported:" << endl;
	cout << "quit" << endl;
	cout << "highscore" << endl;

	StateBase::Show(context);
}